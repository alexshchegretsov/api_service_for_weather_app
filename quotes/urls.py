# -*- coding: utf-8 -*-

from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
# роутер генерирует свои уже готовые урлы, котрые мы подключим с помощью path
router.register('quotes', views.QuoteView)

urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('', views.home, name='home'),
]
