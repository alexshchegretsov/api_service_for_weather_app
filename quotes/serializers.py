# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Quote

class QuotSerializer(serializers.ModelSerializer):
    class Meta:
        # указываем модель для сериализации
        model = Quote
        # выбираем поля для серилизации
        fields = ["id", "title", "description"]