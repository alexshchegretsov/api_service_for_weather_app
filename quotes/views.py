from django.shortcuts import render
from rest_framework import viewsets
from .models import Quote
from .serializers import QuotSerializer

# ModelViewSet скрывает GET,POST,PUT,DELETE
class QuoteView(viewsets.ModelViewSet):
    # для работы с БД
    queryset = Quote.objects.all()
    # для трансляции
    serializer_class = QuotSerializer


def home(request):
    return render(request, 'quotes/home.html', context={"quotes": Quote.objects.all()})